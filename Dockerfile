FROM debian:testing

ENV DEBIAN_FRONTEND=noninteractive

RUN echo 'path-exclude=/usr/share/doc/*' > /etc/dpkg/dpkg.cfg.d/99-exclude-cruft
RUN echo 'path-exclude=/usr/share/man/*' >> /etc/dpkg/dpkg.cfg.d/99-exclude-cruft
RUN echo '#!/bin/sh' > /usr/sbin/policy-rc.d
RUN echo 'exit 101' >> /usr/sbin/policy-rc.d
RUN chmod +x /usr/sbin/policy-rc.d

############### Install apitrace and packages for building

ARG DEBIAN_ARCH
RUN echo deb-src http://deb.debian.org/debian testing main >> /etc/apt/sources.list && \
    apt-get update && \
    apt-get -y install ca-certificates && \
    apt-get -y install --no-install-recommends apitrace ccache && \
    apt-get -y build-dep --no-install-recommends mesa && \
    rm -rf /var/lib/apt/lists

ENTRYPOINT [""]
